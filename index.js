import startLanguageToolServer from '@funboxteam/languagetool-node/lib/startLanguageToolServer.js'
import { toText } from '@inkylabs/remark-utils'
import axios from 'axios'
import chalk from 'chalk'
import Cite from 'citation-js'
import { promises as fs } from 'fs'
import glob from 'globby'
import path from 'path'
import { v4 as uuidv4 } from 'uuid'

const NAME = 'text'

function handleNode (node, ctx) {
  switch (node.type) {
    case 'blockquote':
      handleBlockquote(node, ctx)
      break
    case 'caption':
      handleBlock(node, ctx)
      break
    case 'Cite':
    case 'cite':
      handleCite(node, ctx)
      break
    case 'comment':
      break
    case 'controlspace':
      ctx.output += ' '
      break
    case 'definition':
      handleDefinition(node, ctx)
      break
    case 'emphasis':
      handleBlock(node, ctx)
      break
    case 'figure':
      handleFigure(node, ctx)
      break
    case 'fraktur':
      handleBlock(node, ctx)
      break
    case 'footnote':
      handleFootnote(node, ctx)
      break
    case 'hang':
      handleHang(node, ctx)
      break
    case 'heading':
      handleHeading(node, ctx)
      break
    case 'html':
      break
    case 'image':
      handleImage(node, ctx)
      break
    case 'indexfmt':
      break
    case 'indexstart':
    case 'indexend':
      break
    case 'label':
      handleLabel(node, ctx)
      break
    case 'linebreak':
      handleLinebreak(node, ctx)
      break
    case 'link':
      handleLink(node, ctx)
      break
    case 'list':
      handleList(node, ctx)
      break
    case 'listItem':
      handleListItem(node, ctx)
      break
    case 'medspace':
      ctx.output += ' '
      break
    case 'noindent':
      break
    case 'paragraph':
      handleParagraph(node, ctx)
      break
    case 'attribution':
      handleAttribution(node, ctx)
      break
    case 'ref':
      handleRef(node, ctx)
      break
    case 'root':
      handleBlock(node, ctx)
      ctx.output += '\n'
      break
    case 'strong':
      handleBlock(node, ctx)
      break
    case 'smallcaps':
      handleBlock(node, ctx)
      break
    case 'softhyphen':
      break
    case 'span':
      handleBlock(node, ctx)
      break
    case 'subheading':
      handleSubheading(node, ctx)
      break
    case 'subscript':
      handleBlock(node, ctx)
      break
    case 'superscript':
      handleBlock(node, ctx)
      break
    case 'table':
      handleTable(node, ctx)
      break
    case 'tableBody':
      handleBlock(node, ctx)
      break
    case 'tableCell':
      handleTableCell(node, ctx)
      break
    case 'tableHead':
      handleBlock(node, ctx)
      break
    case 'tableRow':
      handleTableRow(node, ctx)
      break
    case 'tex':
      break
    case 'texenv':
      break
    case 'text':
      handleText(node, ctx)
      break
    case 'underline':
      handleBlock(node, ctx)
      break
    case 'verseref':
      ctx.output += node.value
      break
    default:
      ctx.file.message(`unknown type: ${node.type}`, node.position,
        `${NAME}:no-unknown-type`)
  }
}

function handleBlock (node, ctx, opts) {
  opts = Object.assign({
    wrapped: false
  }, opts)
  const mods = []
  const curly = mods.length && !opts.wrap
  if (curly) ctx.output += '{'
  ctx.output += mods.join('')
  ctx.blockStart = true
  for (const c of node.children) {
    handleNode(c, ctx)
    ctx.blockStart = false
  }
  ctx.blockStart = false
  if (curly) ctx.output += '}'
}

function handleAttribution (node, ctx) {
  // if (ctx.quoting) ctx.output += '”\n'
  ctx.quoting = false
  ctx.output += '—'
  handleBlock(node.labelNode, ctx)
  if (node.contentsNode.children.length) {
    ctx.output += ', '
    handleBlock(node.contentsNode, ctx)
  }
  ctx.output += '\n'
}

function handleBlockquote (node, ctx) {
  if (!ctx.blockStart) ctx.output += '\n\n'
  // ctx.output += '“'
  ctx.quoting = true
  handleBlock(node, ctx)
  // if (ctx.quoting) ctx.output += '”'
  ctx.quoting = false
}

function handleCite (node, ctx) {
  const key = toText(node)
  const source = ctx.sources[key]
  if (!source) {
    ctx.file.message(`unknown source ${key}`, node.position,
      `${NAME}:known-sources`)
    return
  }
  const cite = new Cite()
  const c = cite.set(source).get({
    format: 'string',
    type: 'string',
    style: 'citation-apa'
  })
    .replace(/^<div.*?<div.*?>(.*)<\/div>.*?div>$/s, '$1')
    .replace(/\s+$/, '')
    .replace(/\(n.d.\)\. /g, '') // citation-js adds this if there is no date.
    .replace(/\.$/, '') // We'll add a final period in the text if we want it.
  ctx.output += c

  const pages = node.attributes.p
  if (pages) {
    const bibre = new RegExp(key + '.*?^}$', 'ms')
    const m = source._graph[0].data.match(bibre)
    if (!m) {
      ctx.file.message(`could not parse "${key}" from .bib file`,
        node.position, `${NAME}:parseable-bib`)
      return
    }
    let prefix
    ctx.output += ` ${prefix} ${pages}`
  }
}

function handleDefinition (node, ctx) {
  ctx.output += ctx.blockStart ? '' : '\n\n'
  handleBlock(node.labelNode, ctx)
  ctx.output += '\n'
  handleBlock(node.contentsNode, ctx)
}

function handleFootnote (node, ctx) {
  ctx.footnotes.push(node)
  ctx.output += `[${ctx.footnotes.length}]`
}

function handleHang (node, ctx) {
  handleBlock(node, ctx)
}

function handleHeading (node, ctx) {
  ctx.output += ctx.blockStart ? '' : '\n\n'
  switch (node.depth) {
    case 1:
      ctx.depth = 1
      ctx.output += 'Chapter '
      break
    case 2:
      ctx.depth = 2
      ctx.output += '== '
      break
    case 3:
      ctx.depth = 3
      ctx.output += '=== '
      break
    case 4:
      ctx.depth = 4
      ctx.output += '==== '
      break
    default:
      ctx.file.message(`unhandled heading depth: ${node.depth}`,
        node.position, `${NAME}:known-depth`)
  }
  handleBlock(node, ctx)
  ctx.output += '\n'
}

function handleFigure (node, ctx) {
  ctx.output += ctx.blockStart ? '' : '\n\n'
  handleBlock(node, ctx)
  ctx.output += '\n'
}

function handleImage (node, ctx) {
  return `![${node.alt}](${node.url})`
}

function handleLabel (node, ctx) {
  ctx.labels[node.value] = null
}

function handleRef (node, ctx) {
  ctx.output += `${ctx.refPrefix}{${node.value}}`
}

function handleLinebreak (node, ctx) {
  ctx.output += '\n'
}

function handleLink (node, ctx) {
  const url = toText(node)
  ctx.output += `(${url})`
}

function handleList (node, ctx) {
  handleBlock(node, ctx)
}

function handleListItem (node, ctx) {
  ctx.output += '— '
  handleBlock(node, ctx)
  ctx.output += '\n'
}

function handleParagraph (node, ctx) {
  ctx.output += ctx.blockStart ? '' : '\n\n'
  handleBlock(node, ctx)
}

function handleSubheading (node, ctx) {
  handleBlock(node, ctx)
  ctx.output += '\n'
}

function handleTable (node, ctx) {
  handleBlock(node, ctx)
}

function handleTableCell (node, ctx) {
  handleBlock(node, ctx)
}

function handleTableRow (node, ctx) {
  node.children.forEach((c, i) => {
    handleNode(c, ctx)
    ctx.output += i === node.children.length - 1 ? ' \\\\\n' : ' & '
  })
}

function handleText (node, ctx) {
  ctx.output += node.value
    .replace(/\s+/g, ' ')
}

async function lt (content, { language, ignore, words, wordsfile }) {
  words = new Set(words)
  if (wordsfile) {
    let newWords
    try {
      const filepath = path.join('..', '..', wordsfile)
      newWords = await fs.readFile(filepath, 'utf8')
    } catch (e) {
      console.error(`could not open words file: ${e.message}`)
    }
    (newWords || '#nothing')
      .split(/\s+/g)
      .filter(w => !w.startsWith('#'))
      .forEach(w => words.add(w))
  }

  console.log('Starting languagetool server…')
  let server
  try {
    server = await startLanguageToolServer()
  } catch (e) {
    console.error(`Error starting languagetool server: ${e.message}`)
    return
  }
  console.log('languagetool server started!')
  process.on('SIGINT', () => {
    console.log('killing languagetool server…')
    process.kill(-server.child.pid)
    console.log('languagetool server killed')
    server = null
  })

  for (const c of content) {
    console.log(`Analyzing ${c.filename}…`)
    for (const text of c.text.split(/\n\n+/g)) {
      let res
      try {
        res = await axios({
          url: `http://127.0.0.1:${server.port}/v2/check`,
          method: 'post',
          params: {
            language,
            text
          }
        })
      } catch (e) {
        console.error(`Error making languagetool request: ${e.message}`)
        return
      }
      if (res.status !== 200) {
        console.error(`Error reaching languagetool server: ${res.code}`)
        return
      }
      for (const m of res.data.matches) {
        const badText = text.substr(m.offset, m.length)
        if (m.rule.id === 'MORFOLOGIK_RULE_EN_US' && (
          words.has(badText) ||
         words.has(badText.toLowerCase())
        )) {
          continue
        }
        const ctxt = m.context.text.trim()
        const ig = ignore[m.rule.id] || []
        if (ig.some(re => ctxt.match(re))) continue
        const rpl = m.replacements
          .map(r => `- ${r.value}`)
          .join('\n')
        const txt = text.substr(m.offset, m.length)
        console.error(chalk.red(`${m.message} (${m.rule.id})`))
        console.error(`text: ${ctxt}\n"${txt}"\nreplacements:\n${rpl}`)
      }
    }
  }
}

async function replaceLabels (refPrefix, labels) {
  const files = await glob('genfiles/**/*.txt')
  const refre = new RegExp(refPrefix + '\\{(.*?)\\}', 'g')
  for (const f of files) {
    let text = await fs.readFile(f, 'utf8')
    // text = text.replace(refre, (_, k) => labels[k])
    // This isn't right, but whatevs for now.
    text = text.replace(refre, (_, k) => '1')
    await fs.writeFile(f, text)
  }
}

async function writeText ({
  appendices,
  bibfiles,
  bibliography,
  chapters,
  copyright,
  dedication,
  endorsements,
  foreword,
  halftitlepage,
  indices,
  listoffigures,
  preamble,
  tableofcontents,
  titlepage
}, {
  ignore,
  language,
  languagetool,
  outfile,
  words,
  wordsfile
}) {
  const content = []
  const collect = async (name) => {
    const filepath = path.join('genfiles', `${name}.txt`)
    const text = await fs.readFile(filepath, 'utf8')
    content.push({
      filename: name,
      text
    })
  }

  await collect(endorsements)
  await collect(halftitlepage)
  await collect(titlepage)
  await collect(copyright)
  await collect(dedication)
  if (foreword) await collect(foreword)
  for (const c of chapters) {
    await collect(c)
  }
  for (const c of appendices) {
    await collect(c)
  }

  if (languagetool) {
    await lt(content, {
      ignore,
      language,
      words,
      wordsfile
    })
  }

  await fs.writeFile(outfile, content
    .map(c => c.text)
    .join('\n\n'))
}

export default async (config) => {
  config = Object.assign({
    languagetool: false,
    language: 'en-US',
    words: [],
    wordsfile: null,
    ignore: {}
  }, config)
  const roots = {}
  const refPrefix = uuidv4()
  const labels = {}
  return {
    fileExt: 'txt',
    remarkFilePlugin (opts) {
      this.Compiler = compiler

      const lineMap = {}
      const ctx = {
        lineMap,
        output: '',
        footnotes: [],
        sources: opts.sources,
        refPrefix,
        labels
      }
      function compiler (root, file) {
        roots[file.history[0]] = root
        ctx.file = file
        handleNode(root, ctx)
        ctx.footnotes.forEach((fn, i) => {
          ctx.output += `[${i + 1}] `
          handleBlock(fn, ctx)
        })
        return ctx.output
      }
    },
    async buildAfterFiles (bookConfig) {
      await replaceLabels(refPrefix, labels)
      await writeText(bookConfig, config)
      return true
    }
  }
}
